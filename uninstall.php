<?php
/**
 * Uninstall procedure for the plugin.
 *
 * @package		ChurchAmp_Sundays
 * @version		5.0.0
 * @since			1.0.0
 * @author		Endeavr Media <support@endeavr.com>
 * @copyright		Coppyright (c) 2013, Jason Loftis (jLOFT / Endeavr / ChurchAmp)
 * @link			http://churchamp.com/plugins/sundays
 * @license		http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

/* Make sure we're actually uninstalling the plugin. */
if ( !defined( 'WP_UNINSTALL_PLUGIN' ) )
	wp_die( sprintf( __( '%s should only be called when uninstalling the plugin.', 'churchamp-sundays' ), '<code>' . __FILE__ . '</code>' ) );

/* === Delete plugin options. === */

delete_option( 'plugin_churchamp_sundays' );

/* === Remove capabilities added by the plugin. === */

/* Get the administrator role. */
$role =& get_role( 'administrator' );

/* If the administrator role exists, remove added capabilities for the plugin. */
if ( !empty( $role ) ) {

	$role->remove_cap( 'manage_sundays' );
	$role->remove_cap( 'create_sundays' );
	$role->remove_cap( 'edit_sundays' );
}