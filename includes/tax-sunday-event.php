<?php
/**
 * Taxonomy ( Register Sunday Event Type )
 *
 * @package  		ChurchAmp_Sundays
 * @subpackage  	Includes
 * @version  		5.0.0
 * @since   		1.0.0
 * @author  		Endeavr Media <support@endeavr.com>
 * @copyright  	Coppyright (c) 2013, Jason Loftis (jLOFT / Endeavr / ChurchAmp)
 * @link   		http://churchamp.com/plugins/sundays
 * @license  		http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

/* register and define the taxonomy on the 'init' hook */
/* @example: http://codex.wordpress.org/Function_Reference/register_taxonomy */
add_action( 'init', 'endvr_register_tax_sundayevent' );
function endvr_register_tax_sundayevent() {

	$labels = array(
		'name'                       	=> __( 'Event Type',                           		'churchamp-sundays' ),
		'singular_name'              	=> __( 'Event Type',                            		'churchamp-sundays' ),
		'menu_name'                  	=> __( 'Event Types',                           		'churchamp-sundays' ),
		'name_admin_bar'             	=> __( 'Event Types',                            		'churchamp-sundays' ),
		'search_items'               	=> __( 'Search '.'Event Types'.'',                    	'churchamp-sundays' ),
		'popular_items'              	=> __( 'Popular '.'Event Types'.'',                   	'churchamp-sundays' ),
		'all_items'                  	=> __( 'All '.'Event Types'.'',                       	'churchamp-sundays' ),
		'edit_item'                  	=> __( 'Edit '.'Event Type'.'',                       	'churchamp-sundays' ),
		'view_item'                  	=> __( 'View '.'Event Type'.'',                       	'churchamp-sundays' ),
		'update_item'                	=> __( 'Update '.'Event Type'.'',                     	'churchamp-sundays' ),
		'add_new_item'               	=> __( 'Add New '.'Event Type'.'',                    	'churchamp-sundays' ),
		'new_item_name'             	=> __( 'New '.'Event Type'.' Name',                	'churchamp-sundays' ),
		'separate_items_with_commas' 	=> __( 'Separate '.'Event Types'.' with Commas',      	'churchamp-sundays' ),
		'add_or_remove_items'        	=> __( 'Add or Remove '.'Event Types'.'',             	'churchamp-sundays' ),
		'choose_from_most_used'      	=> __( 'Choose from the Most Used '.'Event Types'.'',	'churchamp-sundays' ),
	);
	/* only 2 caps are needed: 'manage_sundays' and 'edit_sundays'. */
	$capabilities = array(
		'manage_terms' 			=> 'manage_sundays',
		'edit_terms'   			=> 'manage_sundays',
		'delete_terms' 			=> 'manage_sundays',
		'assign_terms' 			=> 'edit_sundays',
	);
	$rewrite = array(
		'slug'         			=> 'sundays/schedule',
		'with_front'   			=> false,
		'hierarchical' 			=> false,
		'ep_mask'      			=> EP_NONE,
	);
	$args = array(
		'public'            		=> true,
		'show_ui'           		=> true,
		'show_in_nav_menus' 		=> true,
		'show_tagcloud'     		=> false,
		'show_admin_column' 		=> true,
		'hierarchical'      		=> true,
		'query_var'         		=> 'sundayevent',
		'capabilities' 			=> $capabilities,
		'rewrite' 				=> $rewrite,
		'labels' 					=> $labels,
	);

	/* register the 'sundayevent' taxonomy. */
	register_taxonomy( 'sundayevent', array( 'sundays' ), $args );
}