Endeavr / ChurchAmp - Sundays Module

=== Plugin Code Source ===
Contributors: jLOFT / Endeavr / ChurchAmp
Donate link: http://churchamp.com/donate
Tags: sunday, post type, taxonomy
Requires at least: 3.5
Tested up to: 3.5.1
Stable tag: 5.0.0
License: http://www.gnu.org/licenses/old-licenses/gpl-2.0.html

A Sundays Plugin for ChurchAmp WordPress Themes (built with custom post types and taxonomies).

== Description ==

**Support Questions:**  The official support forum for this plugin is located at <a href="http://churchamp.com/support">ChurchAmp</a>.

**Plugin Features**

* Sunday Entries:  Allows you to create individual sunday entries and assign them to relevant taxonomies for organization.
* Taxonomies:  Organize your Sunday entries by event type.
* Admin:  Everything is built right into the WordPress admin.  It'll look and feel just like adding posts and pages, so there's not a huge learning curve.
* Custom Fields: Each Sunday entry allows you to publish the title, subtitle, date, summary, and associated events.

**Plugin Dependencies**

* Theme 	=> ChurchAmp: At this time, this plugin is only compatible with ChurchAmps themes.

**Credits**

ChurchAmp and the Sundays plugin are products of <a href="http://endeavr.com">Endeavr</a>, a media studio owned and operated by <a href="http://jloft.com">Jason Loftis (jLOFT)</a>.

== Installation ==

1. Upload `churchamp.sundays` to the `/wp-content/plugins/` directory.
1. Activate the plugin through the 'Plugins' menu in WordPress.
1. Go to "Settings > Permalinks" in the admin and re-save your permalinks.

== Frequently Asked Questions ==

= Why was this plugin created? =

It is a best practice in WordPress development to add functionality through plugins rather than themes. ChurchAmp adheres to that approach by modularizing its features into a suite of plugins. Ideally, the plugins would work with any theme, but so far, we've only built in compatiblity for our own ChurchAmp theme framework.

= How do I use it? =

It works just like posts or pages.  Once the plugin is activated, you'll see a new menu item in the admin called "Sundays".  From there, you can publish new sunday entries (a similar experience to publishing posts or pages) and add new taxonomy entries for series, speaker, and scripture (similar to adding categories).

= I'm getting 404 errors. How can I fix this? =

Just visit "Settings > Permalinks" in your WordPress admin.  It will flush your rewrite rules.  After that, you shouldn't have any 404 issues.

= I don't see the "Sundays" section. =

It should be located in the WordPress admin menu in a section with other ChurchAmp content types right under Comments.  By default, only administrators can see this menu item.  If you are an administrator and can't see it after activating the plugin, deactivate and reactivate the plugin.  This should add the required permissions to your administrator role.

= How can I allow other users to publish Sunday content on my site? =

By default, the "administrator" role is the only role allowed to publish or edit Sunday content.  However, you can install a role management plugin like <a href="http://wordpress.org/extend/plugins/members">Members</a> to give more users access to Sunday content.

The three capabilities you'll need to add to other roles are:

* `manage_sundays` 	=>  	Allows management of the entire Sundays section (only for trusted users).
* `edit_sundays`	=>  	Allows users to edit (not publish) Sunday content.
* `create_sundays`	=>	Allows users to publish Sunday content.

= Where can I get support? =

The official support forum for this plugin is located at <a href="http://churchamp.com/support">ChurchAmp</a>.

== Screenshots ==

1. Admin Screenshot
2. Theme Screenshot

== Changelog ==

**Version 5.0.0**

First documented release.